package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.FlowerNamespace;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController implements XMLParser<Flower> {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public List<Flower> parseXML() {
		if (xmlFileName == null)
			throw new IllegalArgumentException("XML file name is null");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		if (!isXMLValid("input.xsd", xmlFileName))
			throw new IllegalArgumentException("XML is not valid");

		try {
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("Can't process xml securely");
		}

		List<Flower> flowersList = new ArrayList<>();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File(xmlFileName));
			document.getDocumentElement().normalize();
			NodeList flowers = document.getElementsByTagName(FlowerNamespace.FLOWER.getValue());

			for (int j = 0; j < flowers.getLength(); j++) {
				Node node = flowers.item(j);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element flowerElement = (Element) node;
					Element visualParametersElement = (Element) flowerElement
							.getElementsByTagName(FlowerNamespace.VISUAL_PARAMETERS.getValue()).item(0);
					Element growingTipsElement = (Element) flowerElement
							.getElementsByTagName(FlowerNamespace.GROWING_TIPS.getValue()).item(0);

					Flower flower = new Flower.FlowerBuilder()
							.withName(flowerElement.getElementsByTagName(FlowerNamespace.NAME.getValue()).item(0)
									.getTextContent())

							.withSoil(Flower.Soil.fromString(
									flowerElement.getElementsByTagName(FlowerNamespace.SOIL.getValue()).item(0)
											.getTextContent()))

							.withOrigin(flowerElement.getElementsByTagName(FlowerNamespace.ORIGIN.getValue()).item(0)
									.getTextContent())

							.withStemColour(visualParametersElement.getElementsByTagName(FlowerNamespace.STEM_COLOUR.getValue()).item(0)
									.getTextContent())

							.withLeafColour(visualParametersElement.getElementsByTagName(FlowerNamespace.LEAF_COLOUR.getValue()).item(0)
									.getTextContent())

							.withAveLenFlower(new Flower.Measure(
									visualParametersElement.getElementsByTagName(FlowerNamespace.AVE_LEN_FLOWER.getValue()).item(0)
											.getAttributes()
											.getNamedItem(FlowerNamespace.MEASURE.getValue())
											.getTextContent(),
									Integer.parseInt(visualParametersElement.getElementsByTagName(FlowerNamespace.AVE_LEN_FLOWER.getValue()).item(0)
											.getTextContent())))

							.withTemperature(new Flower.Measure(
									growingTipsElement.getElementsByTagName(FlowerNamespace.TEMPERATURE.getValue()).item(0)
											.getAttributes()
											.getNamedItem(FlowerNamespace.MEASURE.getValue())
											.getTextContent(),
									Integer.parseInt(growingTipsElement.getElementsByTagName(FlowerNamespace.TEMPERATURE.getValue()).item(0)
											.getTextContent())))

							.withWatering(new Flower.Measure(
									growingTipsElement.getElementsByTagName(FlowerNamespace.WATERING.getValue()).item(0)
											.getAttributes()
											.getNamedItem(FlowerNamespace.MEASURE.getValue())
											.getTextContent(),
									Integer.parseInt(growingTipsElement.getElementsByTagName(FlowerNamespace.WATERING.getValue()).item(0)
											.getTextContent())))

							.withLighting(Flower.Lighting.fromString(
									growingTipsElement.getElementsByTagName(FlowerNamespace.LIGHTING.getValue()).item(0)
											.getAttributes()
											.getNamedItem(FlowerNamespace.LIGHT_REQUIRING.getValue())
											.getTextContent()))

							.withMultiplying(Flower.Multiplying.fromString(
									flowerElement.getElementsByTagName(FlowerNamespace.MULTIPLYING.getValue()).item(0)
											.getTextContent()))
							.build();
					flowersList.add(flower);
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new RuntimeException("Can't parse " + xmlFileName);
		}
		return flowersList;
	}

	public static void writeToXML(List<Flower> flowers, Path path) {
		if (flowers == null)
			throw new NullPointerException("List not parsed");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;

		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("Can't obtain new document builder");
		}

		Document document = builder.newDocument();
		Element flowersRoot = document.createElement(FlowerNamespace.FLOWERS.getValue());
		flowersRoot.setAttribute("xmlns", FlowerNamespace.XMLNS.getValue());
		flowersRoot.setAttribute("xmlns:xsi", FlowerNamespace.XMLNS_XSI.getValue());
		flowersRoot.setAttribute("xsi:schemaLocation", FlowerNamespace.XSI_SCHEMA_LOCATION.getValue());
		document.appendChild(flowersRoot);

		for (Flower item : flowers) {
			Element flower = document.createElement(FlowerNamespace.FLOWER.getValue());
			flowersRoot.appendChild(flower);

			Element name = document.createElement(FlowerNamespace.NAME.getValue());
			name.setTextContent(item.getName());
			flower.appendChild(name);

			Element soil = document.createElement(FlowerNamespace.SOIL.getValue());
			soil.setTextContent(item.getSoil().getValue());
			flower.appendChild(soil);

			Element origin = document.createElement(FlowerNamespace.ORIGIN.getValue());
			origin.setTextContent(item.getOrigin());
			flower.appendChild(origin);

			Element visualParameters = document.createElement(FlowerNamespace.VISUAL_PARAMETERS.getValue());
			flower.appendChild(visualParameters);

			Element stemColour = document.createElement(FlowerNamespace.STEM_COLOUR.getValue());
			stemColour.setTextContent(item.getStemColour());
			visualParameters.appendChild(stemColour);

			Element leafColour = document.createElement(FlowerNamespace.LEAF_COLOUR.getValue());
			leafColour.setTextContent(item.getLeafColour());
			visualParameters.appendChild(leafColour);

			Element aveLenFlower = document.createElement(FlowerNamespace.AVE_LEN_FLOWER.getValue());
			aveLenFlower.setTextContent(String.valueOf(item.getAveLenFlower().getValue()));
			aveLenFlower.setAttribute(FlowerNamespace.MEASURE.getValue(), item.getAveLenFlower().getUnit());
			visualParameters.appendChild(aveLenFlower);

			Element growingTips = document.createElement(FlowerNamespace.GROWING_TIPS.getValue());
			flower.appendChild(growingTips);

			Element temperature = document.createElement(FlowerNamespace.TEMPERATURE.getValue());
			temperature.setTextContent(String.valueOf(item.getTemperature().getValue()));
			temperature.setAttribute(FlowerNamespace.MEASURE.getValue(), item.getTemperature().getUnit());
			growingTips.appendChild(temperature);

			Element lighting = document.createElement(FlowerNamespace.LIGHTING.getValue());
			lighting.setAttribute(FlowerNamespace.LIGHT_REQUIRING.getValue(), item.getLighting().getValue());
			growingTips.appendChild(lighting);

			Element watering = document.createElement(FlowerNamespace.WATERING.getValue());
			watering.setTextContent(String.valueOf(item.getWatering().getValue()));
			watering.setAttribute(FlowerNamespace.MEASURE.getValue(), item.getWatering().getUnit());
			growingTips.appendChild(watering);

			Element multiplying = document.createElement(FlowerNamespace.MULTIPLYING.getValue());
			multiplying.setTextContent(item.getMultiplying().getValue());
			flower.appendChild(multiplying);
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new RuntimeException("Can't obtain transformer");
		}

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(document);
		StreamResult result = new StreamResult(path.toFile());
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			throw new RuntimeException("Can't transform document to XML");
		}
	}
}
