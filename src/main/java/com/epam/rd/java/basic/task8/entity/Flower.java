package com.epam.rd.java.basic.task8.entity;

import java.util.Arrays;

public final class Flower {
    private final String name;
    private final Soil soil;
    private final String origin;
    private final String stemColour;
    private final String leafColour;
    private final Measure aveLenFlower;
    private final Measure temperature;
    private final Measure watering;
    private final Lighting lighting;
    private final Multiplying multiplying;

    private Flower (FlowerBuilder builder) {
        name = builder.name;
        soil = builder.soil;
        origin = builder.origin;
        stemColour = builder.stemColour;
        leafColour = builder.leafColour;
        aveLenFlower = builder.aveLenFlower;
        temperature = builder.temperature;
        watering = builder.watering;
        lighting = builder.lighting;
        multiplying = builder.multiplying;
    }

    public String getName() {
        return name;
    }

    public Soil getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public Measure getAveLenFlower() {
        return aveLenFlower;
    }

    public Measure getTemperature() {
        return temperature;
    }

    public Measure getWatering() {
        return watering;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public enum Soil {
        PODZOLIC("подзолистая"),
        GROUND("грунтовая"),
        TURF_PODZOLIC("дерново-подзолистая");

        final String value;

        Soil(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Soil fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Soil.values())
                    .filter(x -> x.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse soil: " + s));
        }
    }

    public enum Multiplying {
        LEAVES("листья"),
        CUTTINGS("черенки"),
        SEEDS("семена");

        final String value;

        Multiplying(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Multiplying fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Multiplying.values())
                    .filter(x -> x.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse multiplying: " + s));
        }
    }

    public enum Lighting {
        YES("yes"),
        NO("no");

        final String value;

        Lighting(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static Lighting fromString(String s) throws IllegalArgumentException {
            return Arrays.stream(Lighting.values())
                    .filter(x -> x.value.equals(s))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Can't parse lighting: " + s));
        }
    }

    public static final class Measure {
        private final String unit;
        private int value;

        public Measure(String unit, int value) {
            this.unit = unit;
            this.value = value;
        }

        public String getUnit() {
            return unit;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public static class FlowerBuilder {
        private String name;
        private Soil soil;
        private String origin;
        private String stemColour;
        private String leafColour;
        private Measure aveLenFlower;
        private Measure temperature;
        private Measure watering;
        private Lighting lighting;
        private Multiplying multiplying;

        public Flower build() {
            return new Flower(this);
        }

        public FlowerBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public FlowerBuilder withSoil(Soil soil) {
            this.soil = soil;
            return this;
        }

        public FlowerBuilder withOrigin(String origin) {
            this.origin = origin;
            return this;
        }

        public FlowerBuilder withStemColour(String stemColour) {
            this.stemColour = stemColour;
            return this;
        }

        public FlowerBuilder withLeafColour(String leafColour) {
            this.leafColour = leafColour;
            return this;
        }

        public FlowerBuilder withAveLenFlower(Measure aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
            return this;
        }

        public FlowerBuilder withTemperature(Measure temperature) {
            this.temperature = temperature;
            return this;
        }

        public FlowerBuilder withWatering(Measure watering) {
            this.watering = watering;
            return this;
        }

        public FlowerBuilder withLighting(Lighting lighting) {
            this.lighting = lighting;
            return this;
        }

        public FlowerBuilder withMultiplying(Multiplying multiplying) {
            this.multiplying = multiplying;
            return this;
        }
    }
}
