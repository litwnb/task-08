package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.Sorter;

import java.nio.file.Path;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> flowers = domController.parseXML();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByName(flowers);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToXML(flowers, Path.of(outputXmlFile));
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = saxController.parseXML();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByTempDesc(flowers);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToXML(flowers, Path.of(outputXmlFile));
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = staxController.parseXML();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Sorter.sortFlowersByTempDescAndByName(flowers);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMController.writeToXML(flowers, Path.of(outputXmlFile));
		System.out.println("End");
	}

}
