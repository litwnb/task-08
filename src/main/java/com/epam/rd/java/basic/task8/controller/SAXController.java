package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.FlowerNamespace;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements XMLParser<Flower> {

	private final String xmlFileName;
	private List<Flower> flowers;
	private StringBuilder elementValue;
	private Flower.Measure measureAttribute;
	private Flower.FlowerBuilder flowerBuilder;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void startDocument() {
		flowers = new ArrayList<>();
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		switch (FlowerNamespace.fromString(qName)) {
			case FLOWER:
				flowerBuilder = new Flower.FlowerBuilder();
				break;
			case NAME:
			case SOIL:
			case ORIGIN:
			case STEM_COLOUR:
			case LEAF_COLOUR:
			case MULTIPLYING:
				elementValue = new StringBuilder();
				break;
			case AVE_LEN_FLOWER:
			case TEMPERATURE:
			case WATERING:
				elementValue = new StringBuilder();
				measureAttribute = new Flower.Measure(attributes.getValue(0), 0);
				break;
			case LIGHTING:
				elementValue = new StringBuilder(attributes.getValue(0));
				break;
			default:
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		switch (FlowerNamespace.fromString(qName)) {
			case FLOWER:
				flowers.add(flowerBuilder.build());
				break;
			case NAME:
				flowerBuilder.withName(elementValue.toString());
				break;
			case SOIL:
				flowerBuilder.withSoil(Flower.Soil.fromString(elementValue.toString()));
				break;
			case ORIGIN:
				flowerBuilder.withOrigin(elementValue.toString());
				break;
			case STEM_COLOUR:
				flowerBuilder.withStemColour(elementValue.toString());
				break;
			case LEAF_COLOUR:
				flowerBuilder.withLeafColour(elementValue.toString());
				break;
			case AVE_LEN_FLOWER:
				measureAttribute.setValue(Integer.parseInt(elementValue.toString()));
				flowerBuilder.withAveLenFlower(measureAttribute);
				break;
			case TEMPERATURE:
				measureAttribute.setValue(Integer.parseInt(elementValue.toString()));
				flowerBuilder.withTemperature(measureAttribute);
				break;
			case WATERING:
				measureAttribute.setValue(Integer.parseInt(elementValue.toString()));
				flowerBuilder.withWatering(measureAttribute);
				break;
			case LIGHTING:
				flowerBuilder.withLighting(Flower.Lighting.fromString(elementValue.toString()));
				break;
			case MULTIPLYING:
				flowerBuilder.withMultiplying(Flower.Multiplying.fromString(elementValue.toString()));
				break;
			default:
				break;
		}
	}

	@Override
	public List<Flower> parseXML() {
		if (xmlFileName == null)
			throw new IllegalArgumentException("XML file name is null");

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser;
		if (!isXMLValid("input.xsd", xmlFileName))
			throw new IllegalArgumentException(xmlFileName + " is not valid");

		try {
			parser = factory.newSAXParser();
			parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			parser.parse(new File(xmlFileName), this);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new RuntimeException("Can't parse " + xmlFileName);
		}
		return flowers;
	}

}