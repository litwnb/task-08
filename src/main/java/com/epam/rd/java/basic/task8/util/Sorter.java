package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;
import java.util.List;

public final class Sorter {
    public static final Comparator<Flower> compareFlowersByName = Comparator.comparing(Flower::getName);
    public static final Comparator<Flower> compareFlowersByTempDesc = Comparator
            .comparing(o -> o.getTemperature().getValue(), Comparator.reverseOrder());
    public static final Comparator<Flower> compareFlowersByTempDescAndByName = compareFlowersByTempDesc
            .thenComparing(compareFlowersByName);

    private Sorter() {}

    public static void sortFlowersByName(final List<Flower> flowers) {
        flowers.sort(compareFlowersByName);
    }

    public static void sortFlowersByTempDesc(final List<Flower> flowers) {
        flowers.sort(compareFlowersByTempDesc);
    }

    public static void sortFlowersByTempDescAndByName(final List<Flower> flowers) {
        flowers.sort(compareFlowersByTempDescAndByName);
    }
}
