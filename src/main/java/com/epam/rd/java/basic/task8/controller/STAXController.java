package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.FlowerNamespace;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.util.FlowerNamespace.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements XMLParser<Flower> {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public List<Flower> parseXML() {
		if (xmlFileName == null)
			throw new IllegalArgumentException("XML file name is null");

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		if (!isXMLValid("input.xsd", xmlFileName))
			throw new IllegalArgumentException(xmlFileName + " is not valid");

		XMLEventReader reader;
		try {
			reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
		} catch (XMLStreamException | FileNotFoundException e) {
			throw new RuntimeException("Can't obtain reader for parsing " + xmlFileName);
		}

		List<Flower> flowers = new ArrayList<>();
		Flower.FlowerBuilder flowerBuilder = new Flower.FlowerBuilder();
		try {
			while (reader.hasNext()) {
				XMLEvent event;
				event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					switch (FlowerNamespace.fromString(startElement.getName().getLocalPart())) {
						case FLOWER:
							event = reader.nextEvent();
							flowerBuilder = new Flower.FlowerBuilder();
							break;
						case NAME:
							event = reader.nextEvent();
							flowerBuilder.withName(event.asCharacters().getData());
							break;
						case SOIL:
							event = reader.nextEvent();
							flowerBuilder.withSoil(Flower.Soil.fromString(event.asCharacters().getData()));
							break;
						case ORIGIN:
							event = reader.nextEvent();
							flowerBuilder.withOrigin(event.asCharacters().getData());
							break;
						case STEM_COLOUR:
							event = reader.nextEvent();
							flowerBuilder.withStemColour(event.asCharacters().getData());
							break;
						case LEAF_COLOUR:
							event = reader.nextEvent();
							flowerBuilder.withLeafColour(event.asCharacters().getData());
							break;
						case AVE_LEN_FLOWER:
							event = reader.nextEvent();
							flowerBuilder.withAveLenFlower(new Flower.Measure(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.parseInt(event.asCharacters().getData())));
							break;
						case TEMPERATURE:
							event = reader.nextEvent();
							flowerBuilder.withTemperature(new Flower.Measure(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.parseInt(event.asCharacters().getData())));
							break;
						case LIGHTING:
							event = reader.nextEvent();
							flowerBuilder.withLighting(Flower.Lighting.fromString(
									startElement.getAttributeByName(new QName(LIGHT_REQUIRING.getValue())).getValue()));
							break;
						case WATERING:
							event = reader.nextEvent();
							flowerBuilder.withWatering(new Flower.Measure(
									startElement.getAttributeByName(new QName(MEASURE.getValue())).getValue(),
									Integer.parseInt(event.asCharacters().getData())));
							break;
						case MULTIPLYING:
							event = reader.nextEvent();
							flowerBuilder.withMultiplying(Flower.Multiplying.fromString(event.asCharacters().getData()));
							break;
						default:
							break;
					}
				}
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equals(FLOWER.getValue())) {
						flowers.add(flowerBuilder.build());
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new RuntimeException("Can't read events in " + xmlFileName);
		}
		return flowers;
	}

}